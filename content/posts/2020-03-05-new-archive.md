---
title: "New archive"
date: 2020-03-05T22:40:00Z
author:
  name: Cj Malone
---

The GPDs seem to be the thing most people are interested in. So I've put up a
new [archive](https://360.keepawayfromfire.co.uk/titles/archives/2020-03-05.7z),
it's the same GPDs as last time but this time I've included the PEC files. It's
a Xbox native format, they contain avatar award data. The data is also available
in JSON in the [repo to this site](https://gitlab.com/KAFF/titles/).
