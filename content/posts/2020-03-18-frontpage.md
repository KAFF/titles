---
title: "New front page"
date: 2020-03-18T22:20:00Z
author:
  name: Cj Malone
---

Saying there is a new front page implies there was an old one. There wasn't, but
anyway.

A few days ago on 2020-03-14 I added some popular titles to the front page, I
used data from
[Wikipedia](https://en.wikipedia.org/wiki/List_of_best-selling_Xbox_360_video_games)
not this site, I don't have any stats about what is the most viewed on here.
(Nothing, this site has no traffic it's just a personal project)


![Current homepage](/images/2020/03/18/homepage.png)
