---
title: "Franchises"
date: 2020-03-18T23:00:00Z
author:
  name: Cj Malone
---

I've started adding some data about game
[franchises]({{< ref "/franchises" >}}).

I don't think it's something I'm going to do for every franchise, I'm doing this
manually. However for the ones I have done it for it allows users to quickly see
other games in the same series. I've added it as another link at the bottom of
a game page.
