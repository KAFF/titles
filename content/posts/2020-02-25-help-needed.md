---
title: "Help Needed"
date: 2020-02-25T14:25:00Z
author:
  name: Cj Malone
---

If anyone has data on Xbox 360 publishers, I'd really appreciate it. I'm trying
to get a name for all of the publishers IDs.

The publishers ID is 2 ASCII characters, it's forms part of the title ID. For
example "AV" is the ID for AV-2001, [Activision]({{< ref "titles/AV" >}}).
And Activisions first Xbox 360 title is AV-2001,
[Call of Duty 2]({{< ref "titles/AV/2001" >}}).
