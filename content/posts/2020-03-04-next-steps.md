---
title: "Next Steps"
date: 2020-03-04T11:45:00Z
author:
  name: Cj Malone
---

My next 2 goals are to list Avatar Awards, and Gamer Pictures. Avatar Awards
should be pretty simple, the same as the achievements. In fact I think I already
have the data on my harddrive, just need to parse it. Done.

Gamer Pictures is gonna be harder, as far as I know there is no manifest of
published Gamer Pictures. So I'm gonna have to scrape them :(, there is prior
art of this, it sucks. I can Improve a little, but not much.

Internally Gamer Pictures are 3 uint32s.

 - Title ID (eg 0x415607d1)
 - Large Image ID (eg 0x0002 8000)
 - Small Image ID (eg 0x0001 8000)

Now, these Image IDs are uints, 4 bytes, 32 bits. But the upper 2 bytes, seem
to be flags, for the size of the image. And in all my checks that's the only
thing that changes between the different Image IDs. So it's redundant, and makes
the common Image ID a uint16.

It's still not great, and at the moment I can't get much better. I'll be
grabbing the smaller image, 32px x 32px instead of 64px x 64px. I can possibly
use the non TLS server, to reduce handshakes, but only if I control the client.
If I do this through a web browser it would have to be TLS, or I'd get mixed
content errors for using non TLS data on a TLS site.

But that's it, I think instead of me scrapings 65535 of images, for thousands of
titles I'm going to do it in your browser, at least until it's cached. It's not
ideal, I don't want to put JavaScript on your system. It's probably not even the
best technological solution, but it's the best I can do.
