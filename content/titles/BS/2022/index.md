{
  "idInt": 1112737766,
  "idHex": "425307e6",
  "idA": "BS",
  "idB": 2022,
  "idPretty": "BS-2022",
  "guid": "66acd000-77fe-1000-9115-d802425307e6",
  "title": "Skyrim",
  "tags": ["Popular"],
  "alt_titles": ["The Elder Scrolls", "The Elder Scrolls V", "The Elder Scrolls 5", "TES"],
  "franchises": ["The Elder Scrolls"],
  "franchises_weight": 5
}
