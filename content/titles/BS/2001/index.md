{
  "idInt": 1112737745,
  "idHex": "425307d1",
  "idA": "BS",
  "idB": 2001,
  "idPretty": "BS-2001",
  "guid": "66acd000-77fe-1000-9115-d802425307d1",
  "title": "Oblivion",
  "tags": ["Popular"],
  "alt_titles": ["The Elder Scrolls", "The Elder Scrolls IV", "The Elder Scrolls 4", "TES"],
  "franchises": ["The Elder Scrolls"],
  "franchises_weight": 5
}
