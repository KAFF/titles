{
  "idInt": 1112737785,
  "idHex": "425307f9",
  "idA": "BS",
  "idB": 2041,
  "idPretty": "BS-2041",
  "guid": "66acd000-77fe-1000-9115-d802425307f9",
  "title": "Morrowind",
  "alt_titles": ["The Elder Scrolls", "The Elder Scrolls III", "The Elder Scrolls 3", "TES"],
  "franchises": ["The Elder Scrolls"],
  "franchises_weight": 3
}
