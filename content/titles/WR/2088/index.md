{
  "idInt": 1464993832,
  "idHex": "57520828",
  "idA": "WR",
  "idB": 2088,
  "idPretty": "WR-2088",
  "guid": "66acd000-77fe-1000-9115-d80257520828",
  "title": "Batman™ Arkham Origins",
  "franchises": ["Batman Arkham"],
  "franchises_weight": 3
}
