{
  "idInt": 1464993794,
  "idHex": "57520802",
  "idA": "WR",
  "idB": 2050,
  "idPretty": "WR-2050",
  "guid": "66acd000-77fe-1000-9115-d80257520802",
  "title": "Batman: Arkham City™",
  "tags": ["Popular"],
  "franchises": ["Batman Arkham"],
  "franchises_weight": 2
}
