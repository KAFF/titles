{
  "idInt": 1431504918,
  "idHex": "55530816",
  "idA": "US",
  "idB": 2070,
  "idPretty": "US-2070",
  "guid": "66acd000-77fe-1000-9115-d80255530816",
  "title": "TC’s H.A.W.X",
  "alt_titles": ["Tom Clancy", "Tom Clancys", "Tom Clancy's", "High Altitude Warfare – Experimental"]
}
