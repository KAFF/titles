{
  "idInt": 1431537643,
  "idHex": "555387eb",
  "idA": "US",
  "idB": 34795,
  "idPretty": "US-34795",
  "guid": "66acd000-77fe-1000-9115-d802555387eb",
  "title": "Splinter Cell DA Demo",
  "alt_titles": ["Tom Clancy", "Tom Clancys", "Tom Clancy's", "Splinter Cell", "Double Agent"]
}
