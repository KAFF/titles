{
  "idInt": 1263405020,
  "idHex": "4b4e07dc",
  "idA": "KN",
  "idB": 2012,
  "idPretty": "KN-2012",
  "guid": "66acd000-77fe-1000-9115-d8024b4e07dc",
  "title": "DDR/DS Universe",
  "alt_titles": ["Dance Dance Revolution", "Dancing Stage Universe"]
}
