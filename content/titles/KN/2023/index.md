{
  "idInt": 1263405031,
  "idHex": "4b4e07e7",
  "idA": "KN",
  "idB": 2023,
  "idPretty": "KN-2023",
  "guid": "66acd000-77fe-1000-9115-d8024b4e07e7",
  "title": "DDR/DS Universe 2",
  "alt_titles": ["Dance Dance Revolution 2", "Dancing Stage Universe 2"]
}
