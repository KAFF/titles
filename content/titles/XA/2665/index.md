{
  "idInt": 1480657513,
  "idHex": "58410a69",
  "idA": "XA",
  "idB": 2665,
  "idPretty": "XA-2665",
  "guid": "66acd000-77fe-1000-9115-d80258410a69",
  "title": "Deadliest Warrior",
  "alt_titles": ["Deadliest Warrior", "The Game"],
  "franchises": ["Deadliest Warrior"],
  "franchises_weight": 1
}
