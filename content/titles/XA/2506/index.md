{
  "idInt": 1480657354,
  "idHex": "584109ca",
  "idA": "XA",
  "idB": 2506,
  "idPretty": "XA-2506",
  "guid": "66acd000-77fe-1000-9115-d802584109ca",
  "title": "Call of Duty Classic",
  "alt_titles": ["Cod 1"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 1
}
