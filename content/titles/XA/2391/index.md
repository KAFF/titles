{
  "idInt": 1480657239,
  "idHex": "58410957",
  "idA": "XA",
  "idB": 2391,
  "idPretty": "XA-2391",
  "guid": "66acd000-77fe-1000-9115-d80258410957",
  "title": "Call of Duty Classic",
  "alt_titles": ["Cod 1"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 1
}
