{
  "idInt": 1480660112,
  "idHex": "58411490",
  "idA": "XA",
  "idB": 5264,
  "idPretty": "XA-5264",
  "guid": "66acd000-77fe-1000-9115-d80258411490",
  "title": "DW: Battlegrounds",
  "alt_titles": ["Deadliest Warrior", "Battlegrounds"],
  "franchises": ["Deadliest Warrior"]
}
