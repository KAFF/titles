{
  "idInt": 1480657756,
  "idHex": "58410b5c",
  "idA": "XA",
  "idB": 2908,
  "idPretty": "XA-2908",
  "guid": "66acd000-77fe-1000-9115-d80258410b5c",
  "title": "Deadliest Warrior 2",
  "alt_titles": ["Deadliest Warrior", "Legends"],
  "franchises": ["Deadliest Warrior"],
  "franchises_weight": 2
}
