{
  "idInt": 1297287718,
  "idHex": "4d530a26",
  "idA": "MS",
  "idB": 2598,
  "idPretty": "MS-2598",
  "guid": "66acd000-77fe-1000-9115-d8024d530a26",
  "title": "Gears of War: Judgment",
  "alt_titles": ["GoW"],
  "franchises": ["Gears of War"],
  "franchises_weight": 4
}
