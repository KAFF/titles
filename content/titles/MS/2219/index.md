{
  "idInt": 1297287339,
  "idHex": "4d5308ab",
  "idA": "MS",
  "idB": 2219,
  "idPretty": "MS-2219",
  "guid": "66acd000-77fe-1000-9115-d8024d5308ab",
  "title": "Gears of War 3",
  "alt_titles": ["GoW"],
  "franchises": ["Gears of War"],
  "franchises_weight": 3
}
