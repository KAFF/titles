{
  "idInt": 1297287146,
  "idHex": "4d5307ea",
  "idA": "MS",
  "idB": 2026,
  "idPretty": "MS-2026",
  "guid": "66acd000-77fe-1000-9115-d8024d5307ea",
  "title": "Forza Motorsport 2",
  "franchises": ["Forza", "Forza Motorsport"],
  "franchises_weight": 2
}
