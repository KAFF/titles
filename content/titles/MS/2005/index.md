{
  "idInt": 1297287125,
  "idHex": "4d5307d5",
  "idA": "MS",
  "idB": 2005,
  "idPretty": "MS-2005",
  "guid": "66acd000-77fe-1000-9115-d8024d5307d5",
  "title": "Gears of War",
  "tags": ["Popular"],
  "alt_titles": ["GoW"],
  "franchises": ["Gears of War"],
  "franchises_weight": 1
}
