{
  "idInt": 1297287153,
  "idHex": "4d5307f1",
  "idA": "MS",
  "idB": 2033,
  "idPretty": "MS-2033",
  "guid": "66acd000-77fe-1000-9115-d8024d5307f1",
  "title": "Fable II",
  "franchises": ["Fable"],
  "franchises_weight": 2,
  "tags": ["Popular"]
}
