{
  "idInt": 1297287213,
  "idHex": "4d53082d",
  "idA": "MS",
  "idB": 2093,
  "idPretty": "MS-2093",
  "guid": "66acd000-77fe-1000-9115-d8024d53082d",
  "title": "Gears of War 2",
  "tags": ["Popular"],
  "alt_titles": ["GoW"],
  "franchises": ["Gears of War"],
  "franchises_weight": 2
}
