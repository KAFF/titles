{
  "idInt": 1297319980,
  "idHex": "4d53882c",
  "idA": "MS",
  "idB": 34860,
  "idPretty": "MS-34860",
  "guid": "66acd000-77fe-1000-9115-d8024d53882c",
  "title": "Forza Motorsport Demo",
  "alt_titles": ["Forza Motorsport 2"],
  "franchises": ["Forza", "Forza Motorsport"]
}
