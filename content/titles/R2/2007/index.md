{
  "idInt": 1379010519,
  "idHex": "523207d7",
  "idA": "R2",
  "idB": 2007,
  "idPretty": "R2-2007",
  "guid": "66acd000-77fe-1000-9115-d802523207d7",
  "title": "DW: Ancient Combat",
  "alt_titles": ["Deadliest Warrior", "Ancient Combat"],
  "franchises": ["Deadliest Warrior"],
  "franchises_weight": 3
}
