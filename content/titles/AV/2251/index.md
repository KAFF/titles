{
  "idInt": 1096157387,
  "idHex": "415608cb",
  "idA": "AV",
  "idB": 2251,
  "idPretty": "AV-2251",
  "guid": "66acd000-77fe-1000-9115-d802415608cb",
  "title": "Modern Warfare® 3",
  "alt_titles": ["Cod 8", "Modern Warfare 3", "MW", "MW3"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 8
}
