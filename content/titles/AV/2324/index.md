{
  "idInt": 1096157460,
  "idHex": "41560914",
  "idA": "AV",
  "idB": 2324,
  "idPretty": "AV-2324",
  "guid": "66acd000-77fe-1000-9115-d80241560914",
  "title": "Advanced Warfare",
  "alt_titles": ["Cod 11", "Call of Duty"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 11
}
