{
  "idInt": 1096157379,
  "idHex": "415608c3",
  "idA": "AV",
  "idB": 2243,
  "idPretty": "AV-2243",
  "guid": "66acd000-77fe-1000-9115-d802415608c3",
  "title": "COD: Black Ops II",
  "alt_titles": ["Cod 9", "Call of Duty", "Black Ops 2"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 9
}
