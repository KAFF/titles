{
  "idInt": 1096190002,
  "idHex": "41568832",
  "idA": "AV",
  "idB": 34866,
  "idPretty": "AV-34866",
  "guid": "66acd000-77fe-1000-9115-d80241568832",
  "title": "CoD: World at War Beta",
  "alt_titles": ["Cod 5", "Call of Duty", "WaW"],
  "franchises": ["Call of Duty"]
}
