{
  "idInt": 1096157469,
  "idHex": "4156091d",
  "idA": "AV",
  "idB": 2333,
  "idPretty": "AV-2333",
  "guid": "66acd000-77fe-1000-9115-d8024156091d",
  "title": "COD: Black Ops III",
  "alt_titles": ["Cod 12", "Call of Duty", "Black Ops 3"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 12
}
