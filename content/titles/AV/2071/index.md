{
  "idInt": 1096157207,
  "idHex": "41560817",
  "idA": "AV",
  "idB": 2071,
  "idPretty": "AV-2071",
  "guid": "66acd000-77fe-1000-9115-d80241560817",
  "title": "Modern Warfare® 2",
  "alt_titles": ["Cod 6", "Modern Warfare 2", "MW", "MW2"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 6,
  "tags": ["Popular"]
}
