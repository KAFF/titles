{
  "idInt": 1096157137,
  "idHex": "415607d1",
  "idA": "AV",
  "idB": 2001,
  "idPretty": "AV-2001",
  "guid": "66acd000-77fe-1000-9115-d802415607d1",
  "title": "Call of Duty 2",
  "alt_titles": ["Cod 2"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 2
}
