{
  "idInt": 1096157436,
  "idHex": "415608fc",
  "idA": "AV",
  "idB": 2300,
  "idPretty": "AV-2300",
  "guid": "66acd000-77fe-1000-9115-d802415608fc",
  "title": "Call of Duty®: Ghosts",
  "alt_titles": ["Cod 10"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 10
}
