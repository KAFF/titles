{
  "idInt": 1096157212,
  "idHex": "4156081c",
  "idA": "AV",
  "idB": 2076,
  "idPretty": "AV-2076",
  "guid": "66acd000-77fe-1000-9115-d8024156081c",
  "title": "Call of Duty: WaW",
  "alt_titles": ["Cod 5", "World at War"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 5
}
