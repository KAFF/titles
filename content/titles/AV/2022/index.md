{
  "idInt": 1096157158,
  "idHex": "415607e6",
  "idA": "AV",
  "idB": 2022,
  "idPretty": "AV-2022",
  "guid": "66acd000-77fe-1000-9115-d802415607e6",
  "title": "Call of Duty 4",
  "alt_titles": ["Cod 4", "Modern Warfare", "Modern Warfare 1", "MW", "MW1"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 4,
  "tags": ["Popular"]
}
