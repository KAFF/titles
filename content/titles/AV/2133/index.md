{
  "idInt": 1096157269,
  "idHex": "41560855",
  "idA": "AV",
  "idB": 2133,
  "idPretty": "AV-2133",
  "guid": "66acd000-77fe-1000-9115-d80241560855",
  "title": "Call of Duty Black Ops",
  "alt_titles": ["Cod 7"],
  "franchises": ["Call of Duty"],
  "franchises_weight": 7,
  "tags": ["Popular"]
}
