{
  "idInt": 1414793400,
  "idHex": "545408b8",
  "idA": "TT",
  "idB": 2232,
  "idPretty": "TT-2232",
  "guid": "66acd000-77fe-1000-9115-d802545408b8",
  "title": "GTA: San Andreas",
  "alt_titles": ["Grand Theft Auto"],
  "franchises": ["Grand Theft Auto"],
  "franchises_weight": 1
}
