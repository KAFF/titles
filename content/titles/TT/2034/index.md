{
  "idInt": 1414793202,
  "idHex": "545407f2",
  "idA": "TT",
  "idB": 2034,
  "idPretty": "TT-2034",
  "guid": "66acd000-77fe-1000-9115-d802545407f2",
  "title": "GTA IV",
  "alt_titles": ["Grand Theft Auto", "4"],
  "franchises": ["Grand Theft Auto"],
  "franchises_weight": 2,
  "tags": ["Popular"]
}
