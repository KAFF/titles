{
  "idInt": 1414793383,
  "idHex": "545408a7",
  "idA": "TT",
  "idB": 2215,
  "idPretty": "TT-2215",
  "guid": "66acd000-77fe-1000-9115-d802545408a7",
  "title": "Grand Theft Auto V",
  "alt_titles": ["Grand Theft Auto", "GTA", "5"],
  "franchises": ["Grand Theft Auto"],
  "franchises_weight": 3,
  "tags": ["Popular"]
}
