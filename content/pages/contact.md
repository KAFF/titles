---
title: "Contact"
---

I'm on a bunch of social sites,
[Mastodon](https://mastodon.social/@Cj),
[Micro.blog](https://micro.blog/CjMalone),
[GitHub](https://github.com/Cj-Malone/),
[Twitter](https://twitter.com/SeriousNSerial),
[YouTube](https://www.youtube.com/channel/UCS9rzAtXnxVXX3DYe4LhHgw).

But if you want to reach me, [email](mailto:CjMalone@mail.com?subject=360%20titles%20project) is probably best.

