---
title: "About"
---

This is an archive of Xbox 360 games. What's your favourite? What makes you
nostalgic?

### Similar projects
* [Xbox DB](https://xbox-db.herokuapp.com/games/) for the original Xbox
* [XboxDB](https://xboxdb.altervista.org/) also for Xbox 360 games
