---
title: "Add"
---
If anyone has any details on games that aren't listed, please contact me. At a
minimum I need the id and the name, but ideally I'd have the GPD, SPA and the
output of <code>xextool default.xex</code>.

<form action="mailto:CjMalone@mail.com?subject=Missing%20360%20title"
	method="post" enctype="text/plain">
	<label for="tname">Game Name:</label>
	<input type="text" id="tname" name="Name" placeholder="Call of Duty 2">
	<br>
	<label for="tid">ID:</label>
	<input type="text" id="tid" name="ID" placeholder="415607D1">
	<br>
	<label for="otherinfo" class="optional">Other Info:</label>
	<textarea id="otherinfo" name="Other details"
		style="width: 100%" rows="5"
		placeholder="Links to Marketplace, archive.org, GPD or SPA files"></textarea>
	<br>
	<label for="xexinfo" class="optional">Xex Info:</label>
	<textarea id="xexinfo" name="Details"
		style="width: 100%" rows="5"
		placeholder="xextool default.xex
XexTool v6.6  -  xorloser 2006-2013 (Build Mon Sep 23 14:56:21 2013)
Reading and parsing input xex file...
Xex Info
  Retail
  Uncompressed
  Encrypted
  Title Module
  XGD2 Only
  Xbox360 Logo Data Present
Basefile Info
  Xex Name:           Call of Duty 2
  Load Address:       82000000
  Entry Point:        8228BAB0
  Image Size:          2290000
  Page Size:             10000
  Checksum:           00000000
  Filetime:           4344C307 - Thu Oct 06 07:24:07 2005
  Stack Size:            10000
Regions
  North America
  Japan
  Australia / New Zealand
  Rest of Europe
Allowed Media
  DVD-XGD2 (Xbox360 Original Disc)
Media Id 
  D5 DE 6E 32 04 BD EF 54 D5 1E 6B 9A 7F D5 64 1C 
Encryption Key 
  77 94 53 08 21 36 B9 F5 09 BF 72 C0 7B 25 B0 C5 
LAN Key 
  87 89 45 EF 1D 09 57 06 F0 F0 C0 39 39 C5 72 B4 
TLS Info
  Number of Slots:    64
  Data Size:          0
  Raw Data Address:   00000000
  Raw Data Size:      0
Execution Id
  Media Id:           00000000
  Title Id:           415607D1  (AV-2001)
  Savegame Id:        00000000
  Version:            v0.0.0.1
  Base Version:       v0.0.0.0
  Platform:           0
  Executable Type:    0
  Disc Number:        0
  Number of Discs:    0
Game Ratings
  ESRB:      ESRB_T            06
  PEGI:      PEGI_16           0D
  PEGI-FI:   UNRATED           FF
  PEGI-PT:   UNRATED           FF
  PEGI-BBFC: PEGIBBFC_15       0C
  CERO:      UNRATED           FF
  USK:       UNRATED           FF
  OFLCAU:    OFLC_AU_MA15      06
  OFLCNZ:    OFLC_NZ_MA15      06
  KMRB:      UNRATED           FF
  BRASIL:    BRAZIL_L          FF
  FPB:       UNRATED           FF
  Taiwan:    UNRATED           FF
  Singapore: UNRATED           FF
"></textarea>
	<br>
	<input type="submit" value="Submit">
</form>
