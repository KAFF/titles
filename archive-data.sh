#!/usr/bin/env bash

DATE=$(date +%Y-%m-%d)
find static/titles -name \*.gpd -exec 7z a static/titles/archives/$DATE.7z {} \;

