#!/usr/bin/env bash

# Clear the current metrics file
cat /dev/null > metrics.txt

# Count the titles that I have pages for
AVILABLE_TITLES=$(find content/titles/*/*/index.md | wc -l)

# Count the titles that I have scraped gamer pics for
CURRENT_TITLES=$(find data/titles/*/*/gamerPictures.json | wc -l)

echo 'AvilableTitles $AVILABLE_TITLES' >> metrics.txt
echo 'CurrentTitles $CURRENT_TITLES' >> metrics.txt
